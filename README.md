## Install

To install the project to your system copy and connect the repository locally so that you can push updates you make and pull changes others make. Enter git clone and the repository URL at your command line:
```
git clone https://ruslanlazin@bitbucket.org/ruslanlazin/beveragemaker.git
```
---
## Compile(build)

To compile the code run:
```
javac BeverageMaker.java
```
---

## Run
```
java BeverageMaker
```

You can use your IDE as well to make the process more user friendly.

---
## License


Copyright 2019 Ruslan Lazin

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at [apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

---

## Apache 2.0 License choice explanation

- Apache 2.0 allows users of the software to distribute, modify, or otherwise use software for any purpose, as long as the user complies with the license terms. The terms state that users can’t remove existing copyright, patent, trademarks and attribution notices.
- Recent updates of the license, leading up to Apache 2.0, allow it to be included by reference, instead of listed in every software file, meaning developers don’t need to insert license text into the GUI.
- In addition, this latest version has two patent clauses. The first states the software can be used regardless of software patents which are in effect, without any further obligations. The second states that software users can’t initiate litigation over patent infringement (and if so, they lose their license).
- This license is considered by many to offer the best patent protection among the permissive licenses.
- A limitation of this license for developers is that it requires you to add prominent notifications of any changes you make to a file.