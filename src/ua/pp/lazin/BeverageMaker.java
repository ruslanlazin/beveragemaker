package ua.pp.lazin;

import javax.swing.*;
import java.math.BigDecimal;
import java.util.*;

public class BeverageMaker {

    private static final BigDecimal TAX = BigDecimal.valueOf(1.13);

    public static void main(String[] args) {
        String name = JOptionPane.showInputDialog("Enter your name");
        try {
            Beverage beverage = getBeverage(JOptionPane.showInputDialog("Enter wanted beverage: Coffee(C), Tea(T)"));
            Size size = getSize(JOptionPane.showInputDialog("Enter size: Large(L), Medium(M), Small(S)"));
            Flavoring flavoring = getFlavoring(JOptionPane.showInputDialog("Enter flavoring: " + beverage.allowedFlavorings));
            checkCompatibility(beverage, flavoring);
            BigDecimal price = (size.price.add(flavoring.price)).multiply(TAX);
            JOptionPane.showMessageDialog(null,
                    String.format("For %s, a %s %s, %s flavoring, cost: $%.2f", name, size.name, beverage.name, flavoring.name, price));
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }

    private static void checkCompatibility(Beverage beverage, Flavoring flavoring) throws Exception {
        if (!beverage.allowedFlavorings.contains(flavoring)) {
            throw new Exception(String.format("%s cannot be added to %s", flavoring.name, beverage.name));
        }
    }

    private static Beverage getBeverage(String input) throws Exception {
        switch (input.toLowerCase()) {
            case "c":
            case "coffee":
                return Beverage.COFFEE;
            case "t":
            case "tea":
                return Beverage.TEA;
            default:
                throw new Exception("Incorrect beverage name" + input);
        }
    }

    private static Size getSize(String input) throws Exception {
        switch (input.toLowerCase()) {
            case "large":
            case "l":
                return Size.LARGE;
            case "medium":
            case "m":
                return Size.MEDIUM;
            case "small":
            case "s":
                return Size.SMALL;
            default:
                throw new Exception("Incorrect size: " + input);
        }
    }

    private static Flavoring getFlavoring(String input) throws Exception {
        switch (input.toLowerCase()) {
            case "none":
                return Flavoring.NONE;
            case "vanilla":
            case "v":
                return Flavoring.VANILLA;
            case "chocolate":
            case "c":
                return Flavoring.CHOCOLATE;
            case "lemon":
            case "l":
                return Flavoring.LEMON;
            case "mint":
            case "m":
                return Flavoring.MINT;
            default:
                throw new Exception("Incorrect flavoring: " + input);
        }
    }

    private enum Beverage {
        COFFEE("coffee", List.of(Flavoring.NONE, Flavoring.VANILLA, Flavoring.CHOCOLATE)),
        TEA("tea", List.of(Flavoring.NONE, Flavoring.LEMON, Flavoring.MINT));

        private final List<Flavoring> allowedFlavorings;
        private final String name;

        Beverage(String name, List<Flavoring> flavorings) {
            this.allowedFlavorings = flavorings;
            this.name = name;
        }
    }

    private enum Size {
        LARGE(BigDecimal.valueOf(3.25), "large"),
        MEDIUM(BigDecimal.valueOf(2.50), "medium"),
        SMALL(BigDecimal.valueOf(1.50), "small");

        private final BigDecimal price;
        private final String name;

        Size(BigDecimal price, String name) {
            this.price = price;
            this.name = name;
        }
    }

    private enum Flavoring {
        NONE(BigDecimal.ZERO, "no"),
        VANILLA(BigDecimal.valueOf(0.25), "vanilla"),
        CHOCOLATE(BigDecimal.valueOf(0.75), "chocolate"),
        LEMON(BigDecimal.valueOf(0.25), "lemon"),
        MINT(BigDecimal.valueOf(0.50), "mint");

        private final BigDecimal price;
        private final String name;

        Flavoring(BigDecimal price, String name) {
            this.price = price;
            this.name = name;
        }
    }
}